package com.skillup.automation.pages;


import com.skillup.automation.utils.TimeOuts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CommonPage {

    protected WebDriver driver;

    public CommonPage(WebDriver driver){
        this.driver = driver;
    }

    public String getUrl(){
        return driver.getCurrentUrl();
    }

    public void enterText(String locator, String text){
        WebElement input = driver.findElement(find(locator));
        TimeOuts.sleep();
        input.clear();
        input.sendKeys(text);

    }

    protected static By find(String locator){
        if (locator.contains("//") || locator.contains("./")){
            return By.xpath(locator);
        }
        return By.cssSelector(locator);
    }

    public void click(String locator){
        driver.findElement(find(locator)).click();
    }

}
