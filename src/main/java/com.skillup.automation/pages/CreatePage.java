package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;

public class CreatePage extends CommonPage{

    public CreatePage(WebDriver driver) {
        super(driver);
    }

    public void assertIsOnPage(String expectedUrl){
        waitPageToLoad();
        String currentUrl = getUrl();
        System.out.println(currentUrl);
        Assert.assertTrue(currentUrl.contains(expectedUrl), "Current url is:" + currentUrl);

    }

    public CreatePage waitPageToLoad(){
        WebDriverWait wait = new WebDriverWait(driver, 10000);
        wait.pollingEvery(Duration.ofMillis(1000));
        wait.until(ExpectedConditions.urlContains("create"));
        return this;
    }

}
