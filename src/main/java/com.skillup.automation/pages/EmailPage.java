package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;

import static com.skillup.automation.utils.TimeOuts.*;

public class EmailPage extends CommonPage{

    public EmailPage(WebDriver driver) {
        super(driver);

    }

    public SignUpFinalPage clickConfirmationLink() {
        driver.switchTo().frame(1);
        driver.findElement(find("div.content a")).click();
        sleep();
        return new SignUpFinalPage(driver);

    }

}
