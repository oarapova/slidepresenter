package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;

public class Homepage extends CommonPage{

    private static final String HOMEPAGE_URL = "https://slidepresenter.com/en/";
    private static final String SIGN_UP_BUTTON = ".btn-signup";
    private static final String CLOSE_CHAT_BUTTON = "#collect-chat-widget-close";

    public Homepage(WebDriver driver){
        super(driver);
    }

    public Homepage open(){
        driver.get(HOMEPAGE_URL);
        return this;
    }

    public Homepage clickSignUpButton(){
        click(SIGN_UP_BUTTON);
        return this;
    }

    public Homepage closeChatWindow(){
        click(CLOSE_CHAT_BUTTON);
        return this;

    }

}
