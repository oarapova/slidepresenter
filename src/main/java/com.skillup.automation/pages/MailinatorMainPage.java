package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class MailinatorMainPage extends CommonPage{

    public MailinatorMainPage(WebDriver driver) {
        super(driver);
    }

    public EmailPage getEmailBySubject(String confirmationEmailSubject) {


        List<WebElement> availableEmails = getAvailableEmails(confirmationEmailSubject);
        int numberOfElements = availableEmails.size();
        int timeLimit = 0;
        final int INTERVAL = 10000;

        while (numberOfElements == 0 && timeLimit < 2 * 60 * 1000){
            try {
                Thread.sleep(INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            numberOfElements = getAvailableEmails(confirmationEmailSubject).size();
            timeLimit = timeLimit + INTERVAL;
        }

        availableEmails.get(0).click();


        return new EmailPage(driver);
    }

    private List<WebElement> getAvailableEmails(String confirmationEmailSubject){
        return driver.findElements(find(".table.table-striped.jambo_table tbody tr"))
                .stream()
                .filter(singleEmail -> singleEmail
                        .findElements(find("td")).get(3).getText().equalsIgnoreCase(confirmationEmailSubject)
                ).collect(Collectors.toList());

    }
}
