package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;


public class MailinatorStartPage extends CommonPage{

    private static final String INBOX_FIELD_LOCATOR = "#inboxfield";
    private static final String GO_BUTTON_LOCATOR = "#inboxfield + .input-group-btn > button.btn.btn-default";

    public MailinatorStartPage(WebDriver driver) {
        super(driver);

    }

    public MailinatorMainPage goToInbox(String inboxName) {
        enterText(INBOX_FIELD_LOCATOR, inboxName);
        click(GO_BUTTON_LOCATOR);
        return new MailinatorMainPage(driver);
    }
}

