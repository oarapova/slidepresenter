package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

import static com.skillup.automation.utils.StringGeneratorUtils.randomStringGeneration;

public class SignUpFinalPage extends CommonPage{

    private static final String FIRST_NAME_LOCATOR = "[name='firstName']";
    private static final String LAST_NAME_LOCATOR = "[name='lastName']";
    private static final String COMPANY_SIZE_LOCATOR = "label + div > div > div";
    private static final String PASSWORD_LOCATOR = "[name='password']";
    private static final String PASSWORD_CONFIRMATION_LOCATOR = "[name='passwordConfirmation']";
    private static final String LETS_GO_BUTTON_LOCATOR = "[type='submit']";

    public SignUpFinalPage(WebDriver driver) {
        super(driver);

    }

    public void changeTab() {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());

        String newSignUpTab = tabs.get(1);
        driver.switchTo().window(newSignUpTab);
    }

    public SignUpFinalPage enterFirstName(){
        changeTab();
        enterText(FIRST_NAME_LOCATOR, randomStringGeneration());
        return this;

    }

    public SignUpFinalPage enterLastName(){
        enterText(LAST_NAME_LOCATOR, randomStringGeneration());
        return this;

    }

    public SignUpFinalPage setCompanySize(){
        click(COMPANY_SIZE_LOCATOR);
        driver.findElements(find("ul[role='listbox'] li")).get(0).click();
        return this;

    }


    public SignUpFinalPage enterPassword(String password){
        enterText(PASSWORD_LOCATOR, password);
        return this;

    }

    public SignUpFinalPage enterPasswordConfirmation(String password){
        enterText(PASSWORD_CONFIRMATION_LOCATOR, password);
        return this;

    }

    public SignUpFinalPage clickLetsGoButton(){
        click(LETS_GO_BUTTON_LOCATOR);
        return this;
    }

}
