package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;

import static com.skillup.automation.utils.StringGeneratorUtils.*;

public class SignUpPage extends CommonPage {

    private static final String EMAIL_LOCATOR = "[name='email']";
    private static final String AGREE_CHECKBOX_LOCATOR = ".content form div > i";
    private static final String SIGN_UP_BUTTON_LOCATOR = "[type='submit']";

    public SignUpPage(WebDriver driver){super(driver);}

    public SignUpPage enterEmail(String newUserName){
        enterText(EMAIL_LOCATOR, mailinatorEmailGeneration(newUserName));
        return this;

    }

    public SignUpPage checkAgreeCheckbox(){
        click(AGREE_CHECKBOX_LOCATOR);
        return this;
    }

    public SignUpPage clickSignUpBitton(){
        click(SIGN_UP_BUTTON_LOCATOR);
        return this;
    }




}
