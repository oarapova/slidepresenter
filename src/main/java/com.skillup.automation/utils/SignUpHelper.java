package com.skillup.automation.utils;

import com.skillup.automation.pages.MailinatorStartPage;
import org.openqa.selenium.WebDriver;

public class SignUpHelper {

    private static final String EMAIL_URL = "https://www.mailinator.com";
    private static final String CONFIRMATION_EMAIL_SUBJECT = "SlidePresenter: activation of your account and first steps";


    public static void confirmSignUp(WebDriver driver, String inboxName, String newPassword){
        driver.get(EMAIL_URL);
        new MailinatorStartPage(driver)
                .goToInbox(inboxName)
                .getEmailBySubject(CONFIRMATION_EMAIL_SUBJECT)
                .clickConfirmationLink()
                .enterFirstName()
                .enterLastName()
                .setCompanySize()
                .enterPassword(newPassword)
                .enterPasswordConfirmation(newPassword)
                .clickLetsGoButton();


    }

}
