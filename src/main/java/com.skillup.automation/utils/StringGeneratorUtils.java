package com.skillup.automation.utils;

import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;

public class StringGeneratorUtils {

    public static String randomStringGeneration(){
        RandomStringGenerator randomStringGenerator =
                new RandomStringGenerator.Builder()
                        .withinRange('0', 'z')
                        .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                        .build();
        return randomStringGenerator.generate(10);
    }

    public static String mailinatorEmailGeneration(String randomString){
       return randomString + "@mailinator.com";
    }
}
