package com.skillup.automation.utils;

public class TimeOuts {

    public static void sleep(int timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleep() {
        final int defaulTimeOut = 1000;
        try {
            Thread.sleep(defaulTimeOut);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
