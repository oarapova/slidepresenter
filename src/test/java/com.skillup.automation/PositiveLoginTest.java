package com.skillup.automation;


import org.testng.annotations.Test;

import static com.skillup.automation.utils.SignUpHelper.confirmSignUp;
import static com.skillup.automation.utils.StringGeneratorUtils.randomStringGeneration;

public class PositiveLoginTest extends TestRunner {

    public static String newUserName = randomStringGeneration();
    public static String newPassword = "12@@AAAaaa";

    @Test
    public void positiveSignUp(){

        homepage.open().closeChatWindow().clickSignUpButton();

        signUpPage
                .enterEmail(newUserName)
                .checkAgreeCheckbox()
                .clickSignUpBitton();
        confirmSignUp(driver, newUserName, newPassword);

        createPage.assertIsOnPage("create");
    }


}
