package com.skillup.automation;

import com.skillup.automation.pages.CreatePage;
import com.skillup.automation.pages.Homepage;
import com.skillup.automation.pages.SignUpPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestRunner {
    protected WebDriver driver = null;
    protected Homepage homepage;
    protected SignUpPage signUpPage;
    protected CreatePage createPage;

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("Before suite");
        WebDriverManager.chromedriver().setup();
        //second variant
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("Before class");
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.manage().deleteAllCookies();

        System.out.println("Before method");

        homepage = new Homepage(driver);
        signUpPage = new SignUpPage(driver);
        createPage = new CreatePage(driver);

    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("After method");
//        closeNotUsedTabs();

    }

    @AfterClass
    public void afterClass() {
        System.out.println("After class");
    }

    @AfterSuite
    public void afterSuite() {
        driver.quit();
        System.out.println("After suite");
    }


    private void closeNotUsedTabs(){
        String currentTab = driver.getWindowHandle();

        List<String> allTabs = new ArrayList<String>(driver.getWindowHandles());

        for(int i = 0; i < allTabs.size(); i++){
            String tab = allTabs.get(0);
            if (!tab.equals(currentTab)){
                driver.switchTo().window(tab);
                driver.close();
            }

        }
        driver.switchTo().window(currentTab);

    }

}
